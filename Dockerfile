FROM node:alpine as build
LABEL maintainer="maksim.shirokov2021@icloud.com"
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
